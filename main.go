package main

import (
	"fmt"
)

// Sudoku Solver Exercise
// ======================
//
// The aim is to complete the solve() function so that it returns a completed
// sudoku puzzle. We've setup the execise to use an array to represent the
// state of a puzzle but if you think there is a more suitable data structure
// then feel free to change it.
//
// Please do not spend more than two hours on this, this can be a difficult
// exercise so it's not expected that you complete it within the given time. We
// want to see how you approach coding problems, so please show your working as
// best you can - bonus points if you use git to record your working history!

// REMIS B Notes on the process
// 1. reviewed rules of sudoku game
// 1.1 created new git repository based on request from the comment
// 2. reviewed and analysed task listed in comment bellow
// 3. googled for possible, known sudoku solving solutions
// 3.1 took first found strategy from http://pi.math.cornell.edu/~mec/Summer2009/Mahmood/Solve.html
// 		as a basis for starting
// 4. reviewed provided initial code
// 5. 18:35 made cup of tee and starting to work on solution
// 6. 19:59 starting to work on get list of possible values for every cell - THIS IS HUGE - EXCITING STAGE.
// 		PROBABLY WILL HAVE TO INTORDUCE helper methods for values type to reduce code bloat.
// tool few minute more. Still have a lot to do . You will see where I'm going
// I came to the point where some refactoring should be done.
// commiting the code.

var blockStartIndex = [...]int{0, 3, 6, 27, 30, 33, 54, 57, 60}
var blockShift = [...]int{0, 1, 2, 9, 10, 11, 18, 19, 20}

func solve(puzzle []int) []int {
	filledCols := findMostFilledColumn(puzzle)
	fmt.Printf("FilledCols: %v\n", filledCols)
	filledRows := findMostFilledRows(puzzle)
	fmt.Printf("FilledRows: %v\n", filledRows)
	filledBlocks := findMostFilledBlock(puzzle)
	fmt.Printf("FilledBlocks: %v\n", filledBlocks)

	fillAllPossibleCellValues(puzzle)

	//findForcedCells(puzzle)

	return puzzle
}

type values []int

func (v values) contains(n int) bool {
	for _, value := range v {
		if value == n {
			return true
		}
	}
	return false
}

// time to introduce board structure and assign methods to this structure
// that will remove redundant puzzle argument passing

func fillAllPossibleCellValues(puzzle []int) map[int]values {
	colValues := make(map[int]values)
	for c := 0; c < 9; c++ {
		colValues[c] = getColumnValues(puzzle, c)
		//fmt.Printf("used values for column: %d %v\n", c, colValues[c])
	}
	fmt.Println()

	rowValues := make(map[int]values)
	for r := 0; r < 9; r++ {
		rowValues[r] = getRowValues(puzzle, r)
		//fmt.Printf("used values for row: %d %v\n", r, rowValues[r])
	}
	fmt.Println()

	blockValues := make(map[int]values)
	for b := 0; b < 9; b++ {
		blockValues[b] = getBlockValues(puzzle, b)
		//fmt.Printf("used values for block: %d %v\n", b, blockValues[b])
	}
	fmt.Println()
	//
	//possibleValues := getPossibleValues(getColumnValues(i))

	cellAssignedValues := make(map[int]values, 81)
	cellPossibleValues := make(map[int]values, 81)

	for i := 0; i < 81; i++ {
		row := getRowIndex(i)
		col := getColIndex(i)
		block := getBlockIndex(row, col)
		//fmt.Printf("index: %d row: %d col: %d block: %d\n", i, row, col, block)

		values := make(values, 0)

		// add a method to the structure // Not enough time
		for _, v := range rowValues[row] {
			if !values.contains(v) {
				values = append(values, v)
			}
		}
		for _, v := range colValues[col] {
			if !values.contains(v) {
				values = append(values, v)
			}
		}
		for _, v := range blockValues[block] {
			if !values.contains(v) {
				values = append(values, v)
			}
		}

		cellAssignedValues[i] = values

	}



	for i := 0; i < 81; i ++ {

	}

	cellPossibleValues = cellAssignedValues // reverse should be performed here to remove all assigned values
	return cellPossibleValues
}

func getRowIndex(i int) int {
	return i / 9
}

func getColIndex(i int) int {
	return i - getRowIndex(i) * 9
}

func getBlockIndex(rowIndex, colIndex int) int {
	// getRowBlock
	rowBlock := rowIndex / 3
	colBlock := colIndex / 3
	return rowBlock * 3 + colBlock
}

func appendNot0(values values, newValue int) values {
	if newValue > 0 {
		values = append(values, newValue)
	}
	return values
}

func getColumnValues(puzzle []int, column int) values {
	values := make([]int, 0)
	for r := 0; r < 9; r++ {
		values = appendNot0(values, puzzle[column + r * 9])
	}
	return values
}

func getRowValues(puzzle []int, row int) values {
	values := make([]int, 0)
	startIndex := row * 9
	for i := 0; i < 9; i++ {
		values = appendNot0(values, puzzle[startIndex + i])
	}
	return values
}

func getBlockValues(puzzle []int, block int) values {
	values := make(values, 0)
	startIndex := blockStartIndex[block]
	for i := 0; i < 9; i++ {
		shiftIndex := blockShift[i]
		values = appendNot0(values, puzzle[startIndex + shiftIndex])
	}
	return values
}

func findMostFilledColumn(puzzle []int) map[int]int {
	filledCols := make(map[int]int, 0)
	for c := 0; c < 9; c++ {
		filledCells := 0
		for r := 0; r < 9; r++ {
			if puzzle[c+r*9] > 0 {
				filledCells++
			}
		}
		filledCols[c] = filledCells
	}
	return filledCols
}

func findMostFilledRows(puzzle []int) map[int]int {
	filledRows := make(map[int]int, 9)
	for r := 0; r < 9; r++ {
		filledCells := 0
		for c := 0; c < 9; c++ {
			if puzzle[r*9+c] > 0 {
				filledCells++
			}
		}
		filledRows[r] = filledCells
	}
	return filledRows
}

func findMostFilledBlock(puzzle []int) map[int]int {
	filledBlocks := make(map[int]int, 9)
	for b := 0; b < 9; b++ {
		startIndex := blockStartIndex[b]
		filledCells := 0
		for i := 0; i < 9; i++ {
			shiftIndex := blockShift[i]
			if puzzle[startIndex + shiftIndex] > 0 {
				filledCells++
			}
		}
		filledBlocks[b] = filledCells
	}
	return filledBlocks
}

func compare(puzzle []int, completePuzzle []int) bool {
	for i, n := range puzzle {
		if completePuzzle[i] != n {
			return false
		}
	}
	return true
}

var easySudoku = []int{
	0, 0, 3, 0, 2, 0, 6, 0, 0,
	9, 0, 0, 3, 0, 5, 0, 0, 1,
	0, 0, 1, 8, 0, 6, 4, 0, 0,

	0, 0, 8, 1, 0, 2, 9, 0, 0,
	7, 0, 0, 0, 0, 0, 0, 0, 8,
	0, 0, 6, 7, 0, 8, 2, 0, 0,

	0, 0, 2, 6, 0, 9, 5, 0, 0,
	8, 0, 0, 2, 0, 3, 0, 0, 9,
	0, 0, 5, 0, 1, 0, 3, 0, 0,
}

var hardSudoku = []int{
	6, 0, 0, 0, 0, 0, 1, 5, 0,
	9, 5, 4, 7, 1, 0, 0, 8, 0,
	0, 0, 0, 5, 0, 2, 6, 0, 0,

	8, 0, 0, 0, 9, 4, 0, 0, 6,
	0, 0, 3, 8, 0, 5, 4, 0, 0,
	4, 0, 0, 3, 7, 0, 0, 0, 8,

	0, 0, 6, 9, 0, 3, 0, 0, 0,
	0, 2, 0, 0, 4, 7, 8, 9, 3,
	0, 4, 9, 0, 0, 0, 0, 0, 5,
}

func main() {
	var easyComplete = compare(solve(easySudoku), []int{
		4, 8, 3, 9, 2, 1, 6, 5, 7,
		9, 6, 7, 3, 4, 5, 8, 2, 1,
		2, 5, 1, 8, 7, 6, 4, 9, 3,
		5, 4, 8, 1, 3, 2, 9, 7, 6,
		7, 2, 9, 5, 6, 4, 1, 3, 8,
		1, 3, 6, 7, 9, 8, 2, 4, 5,
		3, 7, 2, 6, 8, 9, 5, 1, 4,
		8, 1, 4, 2, 5, 3, 7, 6, 9,
		6, 9, 5, 4, 1, 7, 3, 8, 2,
	})

	var hardComplete = compare(solve(hardSudoku), []int{
		6, 3, 2, 4, 8, 9, 1, 5, 7,
		9, 5, 4, 7, 1, 6, 3, 8, 2,
		1, 7, 8, 5, 3, 2, 6, 4, 9,

		8, 1, 7, 2, 9, 4, 5, 3, 6,
		2, 9, 3, 8, 6, 5, 4, 7, 1,
		4, 6, 5, 3, 7, 1, 9, 2, 8,

		7, 8, 6, 9, 5, 3, 2, 1, 4,
		5, 2, 1, 6, 4, 7, 8, 9, 3,
		3, 4, 9, 1, 2, 8, 7, 6, 5,
	})

	fmt.Printf("Easy Sudoku Solved?: %v\n", easyComplete)
	fmt.Printf("Hard Sudoku Solved?: %v\n", hardComplete)
}
